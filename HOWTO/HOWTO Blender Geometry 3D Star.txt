Select (Ctrl-A) / Copy (Ctrl-C) / Open Text editor of your choice,
Paste (Ctrl-V) these instructions and print for easier reference)

01) Delete Cube (Del)
02) Add a Rhombicuboctahedron - Shift-A / Mesh / Math Function / Regular Solid / Custom / Rhombicuboctahedron
03) Tab - Edit Mode
04) Poke Faces - Alt-P
05) Unselect All - A
06) Using Vertex Select, Right Mouse Click on the center vertex in any square
07) Select Similar - Shift-G / Amount of Adjacent Faces
08) Scale - S (Drag Mouse to the Right) - Out to your liking
09) Repeat steps 6 -8 on the remaining triangles 
