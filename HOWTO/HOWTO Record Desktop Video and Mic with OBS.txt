
- !!! plug in headset/mic directly at the PC, not through the keyboard !!!
	- missing its drivers on Linux, the keyboard audio ports make funny noises 

- add combined sink for all audio output called "Simlutaneous output to ..."
	-> this seems to be necessary after every system restart
	pacmd load-module module-combine-sink

	# https://askubuntu.com/questions/57319/analog-and-digital-audio-output-at-the-same-time?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa

- in pavucontrol choose that combined sink for playback
	- in PLAYBACK tab!
	- by default it was "Built in Audio Analog Stereo"
		- works only for Firefox audio though? :( what about godot?

- For Godot sounds: in pavucontrol in Input Devices tab
	- show "All Input Deviced"
	- make sure "Monitor of Built-in Audio Analog Stereo" is not muted
		- In OBS this is then picked up by "Desktop Sounds" source
		  (And Firefox sound too, so maybe the combined sink is not even necessary? Or this only works because of that...)
			- Yes INDEED! After system restart, the combined sink is gone, but desktop sounds in OBS still work. nice.
		

- in OBS add Audio Output Capture (Pulse Audio) and set it to that combined "Simlutaneous output to ..." sink too
	-> this seems to be necessary after every system restart
	-> restart OBS for this to take effect

- perhaps to prevent any other problems later, set Playback back to "Built in Audio Analog Stereo"

---> Also see the PNG with the same name as this text file in this dir



- ATTENTION! the mics also pickup any output (if they can hear it. prevent this.). don't get confused when watching the mixer.


###


