
- In Blender, select an object, switch to Texture Paint mode. 
	- Add simple UV
	- Add diffuse color. This adds a new image.
	- If not visible yet, add another view and switch it to UV/Image Editor.
		- select the Diffuse Color image created from the object
		- switch mode from View to Paint
		- View -> Tool Shelf
	- After painting, save the image as file

- Use "Better Collada Exporter" add-on from Godot for Blender
	- In Blender User Preferences -> Install from File
	- Then enable Better Collada and Godot Export Manager

- When Exporting with File -> Export ->"Better Collada" configure it to only export selected objects and to copy images
	- Sometimes, when an image has already been copied during export, it won't be overriden. Delete it manually and export again.

- When Importing in Godot: Import the DAE file as Scene
	- Select the file in the file explorer (left) and configure it in the Import tab (right)
		- Give it a proper name in "Root Name"
		- Node: Storage: Single Scene
		- Materials: Storage: Built in
	- Then instance it from the file explorer into the scene where it is needed
		- Textures should be present

-----

For simple external texture:
- create texture PNG in Gimp
- load it in the Material->Texture in Blender
- it will be exported to Godot by Better Collada exporter
	-> check the HOWTO above for the necessary export flags to make it work
