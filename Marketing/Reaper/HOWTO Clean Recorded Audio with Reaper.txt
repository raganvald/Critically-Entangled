
- Record video in OBS as mp4 (Reaper can import that)

- Open prepared Reaper project
- Drag video file into the prepared audio track
- Cleaning FX are on the master track and applied automatically
- Preview the audio track (listen to it...) 
- File -> Render the clean audio to output file
	- Linux Reaper can't export videos yet, it seems.

- Use Kdenlive to merge new audio track with the video.
	-> see Kdenlive sibling dir
	
OR

- Directly export Video from Reaper
	- MP4
		- Audio: 24 PCM  (AAC doesn't work...)
		- makes much larger files than above Kdenlive way though...
