# Critically Entangled

An experimental Action RTS about extreme micro management, severe lack of time and ultimate balance.
The players attention and focus is the ultimate resource. 

## Devlog

Follow development and find the occasional tutorial: https://www.youtube.com/channel/UCHG2A5U7QLDVYQ2r2KZuPAA

## Built With

* [Godot Engine](https://godotengine.org/) - The game engine
* [Blender](https://www.blender.org/) - 3D modelling
* [Gimp](https://www.gimp.org/) - 2D graphics

## Authors

* **Lukas Sägesser, ScyDev GmbH** - [ScyDev GmbH](https://www.scydev.ch)

## License

This project is licensed under the GPLv3 License - see the [gpl.txt](gpl.txt) file for details


