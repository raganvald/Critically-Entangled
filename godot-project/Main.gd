#
#    This file is part of Critically Entangled.
#    Copyright (c) 2018 Lukas Sägesser, ScyDev GmbH, Switzerland.
#
#    Critically Entangled is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Critically Entangled is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Critically Entangled.  If not, see <http://www.gnu.org/licenses/>.
#

extends Spatial

onready var CAMERAMAN = get_node("/root/Main/CameraMan")
onready var START = get_node("/root/Main/CameraMan/Camera/Start")
onready var HUD = get_node("/root/Main/CameraMan/Camera/HUD")
onready var LOBBY = get_node("/root/Main/CameraMan/Camera/Start/Lobby")

var testingMode = true
var testingModePlayMusic = false
var testingModeGenerateFog = false

var settingsFilePath = "user://CriticallyEntangledSettings.cfg" # Linux: ~/.local/share/godot/app_userdata

var groundScene = preload("res://map_objects/Ground.tscn")
var rock1Scene = preload("res://map_objects/Map_Rock_1.tscn")
var rock2Scene = preload("res://map_objects/Map_Rock_2.tscn")
var rock3Scene = preload("res://map_objects/Map_Rock_3.tscn")
var rock4Scene = preload("res://map_objects/Map_Rock_4.tscn")
var rockScenes = [
	rock1Scene,
	rock2Scene,
	rock3Scene,
	rock4Scene
]

var unitCollectorScene = preload("res://Unit_Collector.tscn")
var unitPlasmaScene = preload("res://Unit_Plasma.tscn")
var unitRailScene = preload("res://Unit_Rail.tscn")
var unitSlingerScene = preload("res://Unit_Slinger.tscn")
var unitLaserScene = preload("res://Unit_Laser.tscn")

var powerupHealthScene = preload("res://map_objects/Powerup_Health.tscn")
var powerupTierScene = preload("res://map_objects/Powerup_Tier.tscn")
var powerupScenes = [
	powerupHealthScene,
	powerupTierScene
]

var unitTypes = {
	"COLLECTOR": 	{"scene": unitCollectorScene, 	"price": 0},
	"PLASMA": 		{"scene": unitPlasmaScene, 		"price": 100},
	"RAIL": 		{"scene": unitRailScene, 		"price": 250},
	"SLINGER":		{"scene": unitSlingerScene, 	"price": 150},
	"LASER": 		{"scene": unitLaserScene, 		"price": 100}
}

var mapWidthPerPlayer = 100 # used for setting the scale of the MeshInstance of OneGround
var mapWidth = 0
var rockCountPerPlayer = 25

var fogDispersalDistance = 60
var fogDispersalTickRate = 0.2
var fogGrowAmount = 0.015 # the amount of alpha a fog tile gains back every tick

var mapObstacleRockScale = Vector3(7, 7, 7)
var spawnPointDistanceFromBorder = 40

var gameModeEntangledSpawning = true

var playersFromServer = null
var startPositions = {}

var selectedUnitName = null
var unitSpawnOffset = Vector3(0, 0.1, 0)
var unitSpawnRotation = Vector3(0, deg2rad(-90), 0) # we want units to be spawned sidewas, for better visibility

var cameraZoomSpeed = 10
var zoomSpeedHeightAdjustFactor = 0.035 # the further away the cam is, the faster it should move
var cameraMinHeight = 20
var cameraMaxHeight = 40
var cameraRotateSpeed = 1
var cameraMoveSpeed = 1
var cameraOffset = Vector3(0, self.cameraMinHeight, 30) # offset of camera from point that should be viewed by it
var cameraDefaultDistance = 20

var resources = 100
var resourceTickTimer = null
var resourceTickRate = 1.0
var resourceTickAmount = 5.0 # must be initialized as float so it will divide properly between players

var tier = 0
var tierGrowthMultiplier = 0.333

var powerupTickTimer = null
var powerupTickRate = 2 * 60.0

var weaponDamageToRocksMultiplier = 0.01
var collisionDamage = 1.0

var started = false

var playersMainUnit = null
var lastDamagedUnitName = null

var hotkeyGroups = {
					1: null,
					2: null,
					3: null,
					4: null,
					5: null,
					6: null,
					7: null,
					8: null,
					9: null,
					0: null,
}

func _ready():
	if (self.testingMode == true):
		self.resources = 10000
		self.resourceTickAmount = 1000
		self.powerupTickRate = 5.0
		self.mapWidthPerPlayer *= 0.5
		self.rockCountPerPlayer *= 0.3
		self.tier = 1
		AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Master"), -20) # because on linux Godot doesn't react to OS volume control
		self.weaponDamageToRocksMultiplier = 0.5
	
	self.resourceTickTimer = Timer.new()
	self.resourceTickTimer.connect("timeout", self, "_on_resourceTickTimer_timeout")
	self.resourceTickTimer.wait_time = self.resourceTickRate
	self.add_child(resourceTickTimer)
	
	self.powerupTickTimer = Timer.new()
	self.powerupTickTimer.connect("timeout", self, "_on_powerupTickTimer_timeout")
	self.powerupTickTimer.wait_time = self.powerupTickRate
	self.add_child(powerupTickTimer)
	
	HUD.updateUnitPrices(self.unitTypes)
	HUD.updateTierCounter(self.tier) # to init all players at tier 1 or maybe a higher one if set in server options
	self.rpc("updatePlayerTier", getNetId(), self.tier) 

func _physics_process(delta):
	# allow cam movement always
	self.handleCameraInput()
		
	# allow other inputs only if game running
	if (self.started == true):
		self.handleHotkeyGroupInput()
		
		if (self.selectedUnitName != null):
			self.handleUnitInput()
		
		if (Input.is_action_just_released("ui_show_last_damaged")):
			var damagedUnit = self.getLastDamagedUnit()
			if (damagedUnit != null):
				CAMERAMAN.centerOnUnit(damagedUnit)
		
		if (Input.is_action_just_released("ui_show_collector")):
			CAMERAMAN.centerOnUnit(self.playersMainUnit)

func handleInputEvent(clickedObject, camera, event, click_position, click_normal, shape_idx):
	if (event.is_class("InputEventMouseButton") && event.pressed == false):
		print("InputEventMouseButton ",event.button_index)
		
		# allow zooming always
		if (event.button_index == 4): # mouse wheel up, zoom in
			var direction = (click_position - CAMERAMAN.translation).normalized()
			var currCamHeight = CAMERAMAN.translation.y
			if (currCamHeight >= self.cameraMinHeight):
				CAMERAMAN.apply_impulse(Vector3(0, 0, 0), (direction) * self.cameraZoomSpeed * (currCamHeight * self.zoomSpeedHeightAdjustFactor))
		
		if (event.button_index == 5): # mouse wheel down, zoom out
			var direction = (click_position - CAMERAMAN.translation).normalized()
			var currCamHeight = CAMERAMAN.translation.y
			var cameraTierMaxHeight = self.getCameraMaxHeightForTier()
			if (currCamHeight < cameraTierMaxHeight): # self.cameraMaxHeight
				CAMERAMAN.apply_impulse(Vector3(0, 0, 0), ((direction * -1)) * self.cameraZoomSpeed * (currCamHeight * self.zoomSpeedHeightAdjustFactor))
		
		# allow click inputs only if game running
		if (self.started == true):
				if (event.button_index == 1): # left click, select
					if (clickedObject.get("gameObjectType") == "UNIT"):
						#print("_on_Tank_input_event",event, click_position)
						if (clickedObject.get_network_master() == getNetId()):
							self.selectUnit(clickedObject)
					if (clickedObject.get("gameObjectType") == "GROUND"):
						var units = get_node("/root/Main").deselectAllUnits()
				
				if (event.button_index == 2): # right click, move
					var selectedUnit = self.getUnitByName(selectedUnitName)
					if (selectedUnit != null):
	#					self.playSoundConfirmClick()
						self.playSoundHudButtonClick()
						selectedUnit.setDestinationPoint(click_position)

func handleUnitInput():
	var selectedUnit = self.getUnitByName(selectedUnitName)
	if (selectedUnit != null):
		if (selectedUnit.weaponType == selectedUnit.WEAPON_TYPE_PROJECTILE):
			if (Input.is_action_just_released("ui_shoot")):
				selectedUnit.prepareWeapon(getNetId())
		if (selectedUnit.weaponType == selectedUnit.WEAPON_TYPE_HITSCAN):
			if (Input.is_action_pressed("ui_shoot")):
				selectedUnit.prepareWeapon(getNetId())
			elif (Input.is_action_just_released("ui_shoot")):
				selectedUnit.rpc("stopFiring", getNetId())

func handleCameraInput():
	if Input.is_action_pressed("ui_cam_turn_left"):
		CAMERAMAN.set_angular_velocity(Vector3(0, cameraRotateSpeed, 0))
		
	if Input.is_action_pressed("ui_cam_turn_right"):
		CAMERAMAN.set_angular_velocity(Vector3(0, -cameraRotateSpeed, 0))
		
	if Input.is_action_pressed("ui_left"):
		var cameraManRotation = CAMERAMAN.rotation
		var direction = Vector3(sin(cameraManRotation.y), 0, cos(cameraManRotation.y)).normalized() * cameraMoveSpeed
		direction = direction.rotated(Vector3(0, 1, 0), deg2rad(-90))
		CAMERAMAN.apply_impulse(Vector3(0, 0, 0), direction)
		
	if Input.is_action_pressed("ui_right"):
		var cameraManRotation = CAMERAMAN.rotation
		var direction = Vector3(sin(cameraManRotation.y), 0, cos(cameraManRotation.y)).normalized() * cameraMoveSpeed
		direction = direction.rotated(Vector3(0, 1, 0), deg2rad(270))
		CAMERAMAN.apply_impulse(Vector3(0, 0, 0), -direction)
		
	if Input.is_action_pressed("ui_up"):
		var cameraManRotation = CAMERAMAN.rotation
		var direction = Vector3(sin(cameraManRotation.y), 0, cos(cameraManRotation.y)).normalized() * cameraMoveSpeed
		CAMERAMAN.apply_impulse(Vector3(0, 0, 0), -direction)
		
	if Input.is_action_pressed("ui_down"):
		var cameraManRotation = CAMERAMAN.rotation
		var direction = Vector3(sin(cameraManRotation.y), 0, cos(cameraManRotation.y)).normalized() * cameraMoveSpeed 
		CAMERAMAN.apply_impulse(Vector3(0, 0, 0), direction)
	
	# prevent cam from bouncing into the ground
	if (CAMERAMAN.translation.y < self.cameraMinHeight): 
		CAMERAMAN.translation.y = self.cameraMinHeight
		CAMERAMAN.linear_velocity = Vector3(0, 0, 0)
	# prevent cam from going higher
	var cameraTierMaxHeight = self.getCameraMaxHeightForTier()
	if (CAMERAMAN.translation.y > cameraTierMaxHeight): # self.cameraMaxHeight
		CAMERAMAN.translation.y = cameraTierMaxHeight
		CAMERAMAN.linear_velocity = Vector3(0, 0, 0)

func handleHotkeyGroupInput():
	var hotkeyUnit = null
	if (selectedUnitName != null):
		var selectedUnit = self.getUnitByName(selectedUnitName)
		
		if (Input.is_action_just_released("ui_group_1")):
			if (Input.is_action_pressed("ui_control") && selectedUnit != null):
				self.hotkeyGroups[1] = selectedUnit.get_name()
			else: 
				hotkeyUnit = get_node("/root/Main/Units/"+str(self.hotkeyGroups[1]))
		if (Input.is_action_just_released("ui_group_2")):
			if (Input.is_action_pressed("ui_control") && selectedUnit != null):
				self.hotkeyGroups[2] = selectedUnit.get_name()
			else: 
				hotkeyUnit = get_node("/root/Main/Units/"+str(self.hotkeyGroups[2]))
		if (Input.is_action_just_released("ui_group_3")):
			if (Input.is_action_pressed("ui_control") && selectedUnit != null):
				self.hotkeyGroups[3] = selectedUnit.get_name()
			else: 
				hotkeyUnit = get_node("/root/Main/Units/"+str(self.hotkeyGroups[3]))
		if (Input.is_action_just_released("ui_group_4")):
			if (Input.is_action_pressed("ui_control") && selectedUnit != null):
				self.hotkeyGroups[4] = selectedUnit.get_name()
			else: 
				hotkeyUnit = get_node("/root/Main/Units/"+str(self.hotkeyGroups[4]))
		if (Input.is_action_just_released("ui_group_5")):
			if (Input.is_action_pressed("ui_control") && selectedUnit != null):
				self.hotkeyGroups[5] = selectedUnit.get_name()
			else: 
				hotkeyUnit = get_node("/root/Main/Units/"+str(self.hotkeyGroups[5]))
		if (Input.is_action_just_released("ui_group_6")):
			if (Input.is_action_pressed("ui_control") && selectedUnit != null):
				self.hotkeyGroups[6] = selectedUnit.get_name()
			else: 
				hotkeyUnit = get_node("/root/Main/Units/"+str(self.hotkeyGroups[6]))
		if (Input.is_action_just_released("ui_group_7")):
			if (Input.is_action_pressed("ui_control") && selectedUnit != null):
				self.hotkeyGroups[7] = selectedUnit.get_name()
			else: 
				hotkeyUnit = get_node("/root/Main/Units/"+str(self.hotkeyGroups[7]))
		if (Input.is_action_just_released("ui_group_8")):
			if (Input.is_action_pressed("ui_control") && selectedUnit != null):
				self.hotkeyGroups[8] = selectedUnit.get_name()
			else: 
				hotkeyUnit = get_node("/root/Main/Units/"+str(self.hotkeyGroups[8]))
		if (Input.is_action_just_released("ui_group_9")):
			if (Input.is_action_pressed("ui_control") && selectedUnit != null):
				self.hotkeyGroups[9] = selectedUnit.get_name()
			else: 
				hotkeyUnit = get_node("/root/Main/Units/"+str(self.hotkeyGroups[9]))
		if (Input.is_action_just_released("ui_group_0")):
			if (Input.is_action_pressed("ui_control") && selectedUnit != null):
				self.hotkeyGroups[0] = selectedUnit.get_name()
			else: 
				hotkeyUnit = get_node("/root/Main/Units/"+str(self.hotkeyGroups[0]))
		
		if (hotkeyUnit != null):
			self.selectUnit(hotkeyUnit)
			
			# detect double tap of hotkey and move cam to unit
			if (get_node("/root/Main/Utils/UnitSelectTimer").get_time_left() > 0):
				CAMERAMAN.centerOnUnit(hotkeyUnit)
			else:
				get_node("/root/Main/Utils/UnitSelectTimer").start()

func getCameraMaxHeightForTier():
	return int(self.cameraMaxHeight + (self.cameraMaxHeight * (self.tier * self.tierGrowthMultiplier)))

func getUnitByName(name):
	return get_node("Units/"+str(name))

func produceUnit(unitType, player_id, position, unitName):
	if (self.resources >= unitTypes[unitType]["price"]):
		self.playSoundHudButtonClick()
		
		self.rpc("spawnUnit", unitType, player_id, position, unitName, self.tier)
		
		# spawn entangled unit for other player
		if (self.gameModeEntangledSpawning == true):
			for player in self.playersFromServer.values():
				if (player["id"] != getNetId()):
					var playersCollector = self.getCollectorForPlayer(player["id"])
					var entangledUnitPosition = self.calculateUnitSpawnPosition(playersCollector)
					var entangledUnitName = unitType+"_p"+str(player["id"])+"_"+str(randf())
					var playerTier = self.playersFromServer[player_id]["tier"]
					self.rpc("spawnUnit", unitType, player["id"], entangledUnitPosition, entangledUnitName, playerTier)
			
		self.spendResources(unitTypes[unitType]["price"])
	else:
		self.playSoundDenyClick()

sync func spawnUnit(unitType, player_id, position, unitName, unitTier):
	var newUnit = self.unitTypes[unitType]["scene"].instance()
	
	newUnit.set_network_master(player_id)
	newUnit.translation = position + self.unitSpawnOffset
	newUnit.rotation = self.unitSpawnRotation 
	newUnit.name = unitName
	newUnit.tier = unitTier
	
	newUnit.setMaterial(self.playersFromServer[player_id]["material"])
	
	get_node("/root/Main/Units").add_child(newUnit)
	
	# play spawn sound
	newUnit.playSound("unit_spawn")
	
	# nudge the unit a little bit to make physics kick in and prevent multiple units spawned inside each other
	newUnit.apply_impulse(Vector3(0, 0, 0), Vector3(0.2, 0, 0.2))
	
	return newUnit

func calculateUnitSpawnPosition(collectorUnit):
	var unitPosition = collectorUnit.translation 
	unitPosition.x += collectorUnit.get_node("CollisionShape").shape.height * 2.5
	return unitPosition

func selectUnit(unit):
	self.playSoundHudButtonClick()
	var selectedUnit = self.getUnitByName(selectedUnitName)
	
	if (selectedUnit != unit):
		var units = self.deselectAllUnits()
		
		self.selectedUnitName = unit.get_name()
		unit.markAsSelected(true)

func deselectAllUnits():
	self.selectedUnitName = null
	
	var units = get_node("/root/Main/Units").get_children()
	for unit in units:
		unit.markAsSelected(false)

func getMouseTargetPoint():
	# project mouse from viewport onto ground
	# probably only works when called from _physics_process() somewhere higher up, as ray casting needs to happen in that context
	var mousePos = CAMERAMAN.get_node("Camera").get_viewport().get_mouse_position()
	return self.getProjectedTargetPoint(mousePos)

func getCameraTargetPoint():
	# project camera center from viewport onto ground
	# probably only works when called from _physics_process() somewhere higher up, as ray casting needs to happen in that context
	var zeroPos = CAMERAMAN.get_node("Camera").get_viewport().get_size()/2
	return self.getProjectedTargetPoint(zeroPos)

func getProjectedTargetPoint(viewportPosition):
	# project position from viewport onto ground
	# probably only works when called from _physics_process() somewhere higher up, as ray casting needs to happen in that context
	var targetPos = null
	var RAY_LENGTH = 1000
	var camera = CAMERAMAN.get_node("Camera")
	var from = camera.project_ray_origin(viewportPosition)
	var to = from + camera.project_ray_normal(viewportPosition) * RAY_LENGTH
	var space_state = self.get_world().get_direct_space_state()
	var result = space_state.intersect_ray(from, to)
	if (result != null && result.has("collider")):
#		print ("camera ray collided with: ", result["collider"].name, " at position: ", result["position"])
		targetPos = result["position"]
		get_node("/root/Main/Utils/TargetMarker").translation = targetPos
		return targetPos
	else:
#		print ("camera ray found nothing to collide with")
		return null

func setLastDamagedUnit(unit):
	if (unit != null && unit.get_network_master() == self.getNetId()):
		self.lastDamagedUnitName = unit.get_name()
		CAMERAMAN.get_node("DamageCompass").flashDamageCompass(unit)

func getLastDamagedUnit():
	var damagedUnit = get_node("/root/Main/Units/"+str(self.lastDamagedUnitName))
	if (damagedUnit != null):
		return damagedUnit

# this is done locally on each client. it works and is deterministic because it depends on the players list and random seed from the server.
func generateMap():
	var playerCount = self.playersFromServer.size()
	var degPerPlayer = 360 / playerCount
	var mapScale = self.mapWidthPerPlayer * playerCount
	mapWidth = mapScale * 2 # because a default mesh cube is 2x2x2. wtf. so if we scale that by any factor, the actual map width in world space will be twice that. hoorray for logic!
	var rockOffset = -mapWidth / 2
	
	# skybox
	var skyboxScale = get_node("/root/Main/Decoration/Skybox").get_scale()
	skyboxScale.x *= playerCount
	skyboxScale.z *= playerCount
	get_node("/root/Main/Decoration/Skybox").set_scale(skyboxScale)
	var mountainScale = get_node("/root/Main/Decoration/Mountain").get_scale()
	mountainScale.x *= playerCount
	mountainScale.z *= playerCount
	get_node("/root/Main/Decoration/Mountain").set_scale(mountainScale)
	
	# Fog
	var fogOverlapFactor = 1.3 # make fog overlap map border by a bit
	get_node("/root/Main/Fog/FogMesh").set_scale(Vector3(mapScale * fogOverlapFactor, 1, mapScale * fogOverlapFactor))
	get_node("/root/Main/Fog/FogMesh").mesh.set("subdivide_width", playerCount * 100) # not doing anything...
	get_node("/root/Main/Fog/FogMesh").mesh.set("subdivide_depth", playerCount * 100)
	
	# ground and collision border
	get_node("/root/Main/OneGround/MeshInstance").set_scale(Vector3(mapScale, 1, mapScale))
	get_node("/root/Main/MapBorder/CollisionShapeWest").set_scale(Vector3(1, 10, mapScale))
	get_node("/root/Main/MapBorder/CollisionShapeWest").set_translation(Vector3((-mapWidth / 2), 0, 0))
	get_node("/root/Main/MapBorder/CollisionShapeEast").set_scale(Vector3(1, 10, mapScale))
	get_node("/root/Main/MapBorder/CollisionShapeEast").set_translation(Vector3((mapWidth / 2), 0, 0))
	get_node("/root/Main/MapBorder/CollisionShapeNorth").set_scale(Vector3(mapScale, 10, 1))
	get_node("/root/Main/MapBorder/CollisionShapeNorth").set_translation(Vector3(0, 0, (-mapWidth / 2)))
	get_node("/root/Main/MapBorder/CollisionShapeSouth").set_scale(Vector3(mapScale, 10, 1))
	get_node("/root/Main/MapBorder/CollisionShapeSouth").set_translation(Vector3(0, 0, (mapWidth / 2)))
	
	# rock wall
	get_node("/root/Main/MapBorder/WallWest").set_scale(Vector3(1, 1, mapScale/100))
	get_node("/root/Main/MapBorder/WallWest").set_translation(Vector3((-mapWidth / 2), 0, 0))
	for rock in get_node("/root/Main/MapBorder/WallWest").get_children():
		rock.get_node("CollisionShape").queue_free()
	get_node("/root/Main/MapBorder/WallEast").set_scale(Vector3(1, 1, mapScale/100))
	get_node("/root/Main/MapBorder/WallEast").set_translation(Vector3((mapWidth / 2), 0, 0))
	for rock in get_node("/root/Main/MapBorder/WallEast").get_children():
		rock.get_node("CollisionShape").queue_free()
	get_node("/root/Main/MapBorder/WallNorth").set_scale(Vector3(1, 1, mapScale/100)) # scale on z axis instead of x, because its rotated by 90°
	get_node("/root/Main/MapBorder/WallNorth").set_translation(Vector3(0, 0, (-mapWidth / 2)))
	for rock in get_node("/root/Main/MapBorder/WallNorth").get_children():
		rock.get_node("CollisionShape").queue_free()
	get_node("/root/Main/MapBorder/WallSouth").set_scale(Vector3(1, 1, mapScale/100)) # scale on z axis instead of x, because its rotated by 90°
	get_node("/root/Main/MapBorder/WallSouth").set_translation(Vector3(0, 0, (mapWidth / 2)))
	for rock in get_node("/root/Main/MapBorder/WallSouth").get_children():
		rock.get_node("CollisionShape").queue_free()
	
	# spawn deco/obstacle rocks
	for x in range(self.rockCountPerPlayer):
		var rockSceneIndex = randi() % rockScenes.size()
		var scaleMultiplier = (0.1 + randf())
		var rockTranslation = Vector3(rockOffset + (mapWidth * randf()), 0, rockOffset + (mapWidth * randf()))
		self.rpc("spawnRock", rockSceneIndex, scaleMultiplier, rockTranslation)
	
	self.rpc("mirrorRocks", playerCount, degPerPlayer)
	
	# calculate player start positions on a circle around map center
	var addedUpDeg = 0
	var startingPosRadius = (mapWidth / 2) - spawnPointDistanceFromBorder
	for player in self.playersFromServer.values():
		addedUpDeg += degPerPlayer
		var startPos = Vector3(sin(deg2rad(addedUpDeg)) * startingPosRadius, 0, cos(deg2rad(addedUpDeg)) * startingPosRadius)
		startPositions[player["id"]] = startPos
		

sync func spawnRock(rockSceneIndex, scaleMultiplier, translation):
	var rockScene = self.rockScenes[rockSceneIndex]
	var rockTile = rockScene.instance()
	rockTile.scale = self.mapObstacleRockScale * scaleMultiplier
	rockTile.scaleMultiplier = scaleMultiplier
	rockTile.translation = translation
	get_node("/root/Main/Ground/Obstacles").add_child(rockTile)

sync func mirrorRocks(playerCount, degPerPlayer):
	# duplicate and "mirror" (rotate) obstacles for each player, to get a fair map
	for x in range(1, playerCount): # start at one because we skip first player because his rocks are the ones generated above and don't need to be rotated
		var obstacles = get_node("/root/Main/Ground/Obstacles").duplicate()
		obstacles.rotate_y(deg2rad(degPerPlayer * x))
		get_node("/root/Main/Ground").add_child(obstacles)

func _on_resourceTickTimer_timeout():
	self.addResources(self.resourceTickAmount)

func addResources(resourcesAmount):
	self.resources += (resourcesAmount / self.playersFromServer.size())
	HUD.updateResourcesCounter(self.resources)

func spendResources(resourcesAmount):
	self.resources -= resourcesAmount
	HUD.updateResourcesCounter(self.resources)

func _on_powerupTickTimer_timeout():
	if (self.getNetId() == 1):
		var powerupIndex = randi() % powerupScenes.size()
		
		var powerupMapArea = mapWidth - 60
		var powerupOffset = -powerupMapArea / 2
		var powerupTranslation = Vector3(powerupOffset + (powerupMapArea * randf()), 40, powerupOffset + (powerupMapArea * randf()))
		
		self.rpc("spawnPowerup", powerupIndex, powerupTranslation)

sync func spawnPowerup(powerupIndex, powerupTranslation):
	var playerCount = self.playersFromServer.size()
	var degPerPlayer = 360 / playerCount
	
	var newPowerup = self.powerupScenes[powerupIndex].instance()
	newPowerup.set_translation(powerupTranslation)
	get_node("/root/Main/Powerups/").add_child(newPowerup)
	
	# duplicate and "mirror" (rotate) powerup, to get a fair distribution
	for x in range(1, playerCount): # start at one because we skip first player because his rocks are the ones generated above and don't need to be rotated
		var copiedPowerup = newPowerup.duplicate()
		
#		print("orig powerup pos: ", copiedPowerup.get_translation())
		
		# rotate powerup copy around center point
		var transform = copiedPowerup.transform
		var start_position = copiedPowerup.get_translation()
		var pivot_point = -1 * start_position
		var pivot_transform = Transform(transform.basis, pivot_point)
		transform = pivot_transform.rotated(Vector3(0, 1, 0), deg2rad(degPerPlayer * (x-1))) # not sure why -1 is needed here ...
		copiedPowerup.transform = transform
		
#		print("rotated powerup pos: ", copiedPowerup.get_translation())
		
		get_node("/root/Main/Ground").add_child(copiedPowerup)
	
	if (self.testingMode == false):
		self.playSoundPowerupIncoming()

func raiseTier():
	self.tier += 1
	HUD.updateTierCounter(self.tier)
	
#	if (self.testingMode == false):
	self.playSoundLevelUp()
	self.rpc("playSoundOpponentLevelUp")
	
	self.rpc("updatePlayerTier", getNetId(), self.tier)

sync func updatePlayerTier(player_id, playerTier):
	self.playersFromServer[player_id]["tier"] = playerTier

sync func initGame(playersFromServer, randSeed):
	self.playersFromServer = playersFromServer
	seed(randSeed)

sync func startGame():
	START.queue_free()
	HUD.show()
	self.started = true
	
	self.generateMap()
	
	self.resourceTickTimer.start()
	self.powerupTickTimer.start()
	
	get_node("/root/Main").playSoundGameStart()
	
	var startPos = self.startPositions[getNetId()]
	
	var unitName = "COLLECTOR"+"_p"+str(getNetId())
	self.rpc("spawnUnit", "COLLECTOR", getNetId(), startPos, unitName, self.tier)
	self.playersMainUnit = self.getCollectorForPlayer(getNetId())
	
	# set camera position to collector unit
	var collectorPos = self.playersMainUnit.get_translation()
	var camStartPos = collectorPos * (1.0 + (self.cameraDefaultDistance / collectorPos.length())) # add an absolute length to vector
	CAMERAMAN.set_translation(camStartPos)
	CAMERAMAN.look_at(collectorPos, Vector3(0, 1, 0))
	CAMERAMAN.rotation.x = 0 # keep only Y rotation from look_at()
	CAMERAMAN.rotation.z = 0

func getCollectorForPlayer(playerId):
	var unitName = "COLLECTOR"+"_p"+str(playerId)
	return get_node("/root/Main/Units/"+unitName)

func removePlayer(playerId):
	for unit in get_node("/root/Main/Units").get_children():
		if (unit.get_network_master() == playerId):
			unit.destroyUnit()
	
	# if it's this player, end his game
	if (playerId == getNetId()):
		get_node("/root/Main").endGame(false)
		return
	else:
		# for all others, let them know a player was defeated
		self.playerDefeated(playerId)
	
	self.playersFromServer.erase(str(playerId))
	
	# last player remaining in the list wins
	if (self.playersFromServer.size() == 1):
		get_node("/root/Main").endGame(true)

func playerDefeated(playerId):
	var acceptDialog = AcceptDialog.new()
	acceptDialog.get_label().text = "Player defeated: "+self.playersFromServer[playerId]["name"]
	HUD.add_child(acceptDialog)
	acceptDialog.popup_centered()

func endGame(playerWins):
	get_node("/root/Main/Sound/Music").stopSong()
	
	var messageText = ""
	if (playerWins == true):
		messageText = "You win!"
		get_node("/root/Main").playSoundGameWin()
	else:
		messageText = "You lose!"
		get_node("/root/Main").playSoundGameLose()
	
	var acceptDialog = AcceptDialog.new()
	acceptDialog.get_label().text = messageText
	HUD.add_child(acceptDialog)
	acceptDialog.popup_exclusive = true
	acceptDialog.popup_centered()
	
	self.started = false # just use this to prevent player input, but don't pause/freeze the game or kick him from the server
	#get_node("/root/Main/Fog/FogMesh").hide()

func getNetId():
	return get_tree().get_network_unique_id()

func playSoundHudButtonClick():
	get_node("Sound/HudButton").play()

func playSoundMenuButtonClick():
	get_node("Sound/MenuButton").play()

func playSoundConfirmClick():
	get_node("Sound/Confirm").play()

func playSoundDenyClick():
	get_node("Sound/Deny").play()

func playSoundGameStart():
	get_node("Sound/GameStart").play()

func playSoundGameLose():
	get_node("Sound/GameLose").play()

func playSoundGameWin():
	get_node("Sound/GameWin").play()

func playSoundPowerupHealth():
	get_node("Sound/PowerupHealth").play()

func playSoundPowerupIncoming():
	get_node("Sound/Voice/PowerupIncoming").play()

func playSoundLevelUp():
	get_node("Sound/Voice/LevelUp").play()

remote func playSoundOpponentLevelUp():
	get_node("Sound/Voice/OpponentLevelUp").play()
