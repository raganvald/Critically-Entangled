#
#    This file is part of Critically Entangled.
#    Copyright (c) 2018 Lukas Sägesser, ScyDev GmbH, Switzerland.
#
#    Critically Entangled is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Critically Entangled is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Critically Entangled.  If not, see <http://www.gnu.org/licenses/>.
#

extends StaticBody

var gameObjectType = "GROUND"

func _ready():
	pass

func _on_Ground_input_event(camera, event, click_position, click_normal, shape_idx):
	get_node("/root/Main").handleInputEvent(self, camera, event, click_position, click_normal, shape_idx)

func takeDamage(damage):
	pass

func playSound(sample, loop):
	pass

func stopSound():
	pass

