#
#    This file is part of Critically Entangled.
#    Copyright (c) 2018 Lukas Sägesser, ScyDev GmbH, Switzerland.
#
#    Critically Entangled is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Critically Entangled is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Critically Entangled.  If not, see <http://www.gnu.org/licenses/>.
#

extends "res://Projectile.gd"

var projectileWeaponType = "SLINGER"

var initialImpact = false
var alreadyDamagedUnits = {}

func _ready():
	self.get_node("CollisionShape").disabled = true # see explanation below in _on_Area_body_exited()
	self.get_node("Explosion/ExplosionArea/CollisionShape").disabled = true # will be enabled below after the inital impact of the projectile
	
	self.set_max_contacts_reported(1000)
	projectileSpeed = 10
	projectileDamage = 50
	
	timeToLive = 10
	
	self.adjustToTier()


func _on_Area_body_exited(collider):
	print("_on_Area_body_exited: spawnedBy: ",spawnedBy, " collider: ",collider," ",collider.name)
	
	# areas can't collide with Plane collision shape on ground. so we are using the area only to detect
	# when the projectile leaves its unit and then enable the collision shape on the RigidBody to detect all collisions.
	if collider == spawnedBy:
		self.get_node("CollisionShape").disabled = false


func _on_Projectile_Slinger_body_entered(collider):
	print("_on_Projectile_Slinger_body_entered: spawnedBy: ",spawnedBy, " collider: ",collider," ",collider.name)
	if (collider != spawnedBy): # to preventing with its spawning unit at launch
		if (self.initialImpact == false):
			print("collision")
			self.initialImpact = true
			
			# activate the explosion
			self.get_node("Explosion/ExplosionArea/CollisionShape").disabled = false
			self.get_node("Explosion/Explosion").show()
			self.get_node("Explosion/ExplosionTimer").start()
			
			var newEffect = effectScene.instance()
			newEffect.set_translation(self.get_translation())
			get_node("/root/Main/Effects").add_child(newEffect)
			newEffect.playSound("slinger_impact")

func _on_ExplosionArea_body_entered(collider):
	print("explosion collision")
	if (self.alreadyDamagedUnits.has(collider.get_name()) != true):
		self.alreadyDamagedUnits[collider.get_name()] = true
		
		if (collider.get("gameObjectType") == "UNIT"):
			collider.takeDamage(self.projectileDamage)
		elif (collider.get("gameObjectType") == "ROCK"):
			collider.takeDamage(self.projectileDamage * MAIN.weaponDamageToRocksMultiplier)
	else:
		print("unit already damaged")

func _on_ExplosionTimer_timeout():
	self.queue_free()
