#
#    This file is part of Critically Entangled.
#    Copyright (c) 2018 Lukas Sägesser, ScyDev GmbH, Switzerland.
#
#    Critically Entangled is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Critically Entangled is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Critically Entangled.  If not, see <http://www.gnu.org/licenses/>.
#

extends StaticBody

var effectScene = preload("res://Effect.tscn")

var gameObjectType = "ROCK"

var health = 100
var scaleMultiplier = 1.0

func _ready():
	self.health = self.health * self.scaleMultiplier
	
#	self.connect("body_entered", self, "_on_Unit_body_entered") # for collision damage

func _on_Ground_input_event(camera, event, click_position, click_normal, shape_idx):
	get_node("/root/Main").handleInputEvent(self, camera, event, click_position, click_normal, shape_idx)

#func _on_Unit_body_entered(body):
#	var bodyGameObjectType = body.get("gameObjectType")
#	if (bodyGameObjectType != null && (bodyGameObjectType == "UNIT" || bodyGameObjectType == "ROCK")):
#		self.takeDamage(100)

func takeDamage(damage):
#	print("rock health: "+str(self.health)+" rock taking damage: "+str(damage))
	self.health -= damage
	
	if (self.health <= 0):
		var newEffect = effectScene.instance()
		newEffect.set_translation(self.get_translation())
		get_node("/root/Main/Effects").add_child(newEffect)
		newEffect.playSound("explosion")
		newEffect.showParticleEffect("unit_explosion")
		
		self.queue_free()

func playSound(sample, loop):
	pass

func stopSound():
	pass