#
#    This file is part of Critically Entangled.
#    Copyright (c) 2018 Lukas Sägesser, ScyDev GmbH, Switzerland.
#
#    Critically Entangled is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Critically Entangled is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Critically Entangled.  If not, see <http://www.gnu.org/licenses/>.
#

extends Control

onready var MAIN = get_node("/root/Main")

func _ready():
	pass

func spawn(unitType):
	if (MAIN.started == true):
		var unitPosition = get_node("/root/Main").calculateUnitSpawnPosition(get_node("/root/Main").playersMainUnit)
		var unitName = unitType+"_p"+str(get_tree().get_network_unique_id())+"_"+str(randf())
		get_node("/root/Main").produceUnit(unitType, get_tree().get_network_unique_id(), unitPosition, unitName)
		
		return unitName

func _on_SpawnPlasmaButton_pressed():
	self.get_node("Panel/HBoxContainer/SpawnPlasmaButton").release_focus()
	self.spawn("PLASMA")

func _on_SpawnRailButton_pressed():
	self.get_node("Panel/HBoxContainer/SpawnRailButton").release_focus()
	self.spawn("RAIL")

func _on_SpawnSlingerButton_pressed():
	self.get_node("Panel/HBoxContainer/SpawnSlingerButton").release_focus()
	self.spawn("SLINGER")

func _on_SpawnLaserButton_pressed():
	self.get_node("Panel/HBoxContainer/SpawnLaserButton").release_focus()
	self.spawn("LASER")

func updateUnitPrices(unitTypes):
	for unitName in unitTypes.keys():
		var unitNameCapitalized = unitName.capitalize()
		var spawnButton = get_node("Panel/HBoxContainer/Spawn"+unitNameCapitalized+"Button")
		if (spawnButton != null):
			spawnButton.hint_tooltip = str(unitTypes[unitName]["price"])
			spawnButton.text = spawnButton.text+" ("+str(unitTypes[unitName]["price"])+")"

func updateResourcesCounter(resourcesAmount):
	self.get_node("Panel/HBoxContainer/ResourcesCounter").text = str(int(resourcesAmount))

func updateTierCounter(tier):
	self.get_node("Panel/HBoxContainer/TierCounter").text = str(tier + 1) # because tier starts at 0 to make scale calculation simpler

