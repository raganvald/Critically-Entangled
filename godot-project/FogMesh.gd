#
#    This file is part of Critically Entangled.
#    Copyright (c) 2018 Lukas Sägesser, ScyDev GmbH, Switzerland.
#
#    Critically Entangled is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Critically Entangled is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Critically Entangled.  If not, see <http://www.gnu.org/licenses/>.
#

extends MeshInstance

onready var MAIN = get_node("/root/Main")

var fogUpdateTimer: Timer = null

var bitmapSize: int = 256
var bitmapAlpha: float = 0.1

func _ready():
	if (MAIN.testingMode == false || MAIN.testingModeGenerateFog == true):
		self.fogUpdateTimer = Timer.new()
		self.fogUpdateTimer.connect("timeout", self, "_on_fogUpdateTimer_timeout")
		self.fogUpdateTimer.wait_time = MAIN.fogDispersalTickRate
		self.add_child(fogUpdateTimer)
		self.fogUpdateTimer.start()
	else:
		self.hide()

func _on_fogUpdateTimer_timeout():
	var myUnits: Array = Array()
	for unit in MAIN.get_node("Units").get_children():
		if (unit.get_network_master() == MAIN.getNetId()):
			myUnits.append(unit)
	
	var bitmap: Image = Image.new()
	bitmap.create(myUnits.size(), 1, false, Image.FORMAT_RGB8)
#	bitmap.fill(Color(0, 0, 0, self.bitmapAlpha))
	
	bitmap.lock()
#	bitmap.set_pixel(128, 128, Color(1, 0, 0, 1))
#	for unit in myUnits:
	for i in range(myUnits.size()):
		var unit = myUnits[i]
		var x = (unit.translation.x + (MAIN.mapWidth / 2)) / MAIN.mapWidth # bring it to between 0.0 and 1.0
		var z = (unit.translation.z + (MAIN.mapWidth / 2)) / MAIN.mapWidth # so we can store it in a color vec4
		bitmap.set_pixel(i, 0, Color(x, 0, z, 1))
	bitmap.lock()
	
	var texture: ImageTexture = ImageTexture.new()
	texture.create_from_image(bitmap, 0) # 0 = raw. we don't want any blended pixels
	texture.set_size_override(bitmap.get_size()) # set texture to same size as image. we want it pixely, in order to transport exact position values.
	
	# for testing
	var material = get_node("../FogBitmap").material_override
	material.flags_transparent = true # does nothing?
	material.albedo_texture = texture
	get_node("../FogBitmap").material_override = material
	
	self.material_override.set_shader_param("maxRange", MAIN.fogDispersalDistance)
	self.material_override.set_shader_param("mapWidth", MAIN.mapWidth)
	self.material_override.set_shader_param("unitPositions", texture)

