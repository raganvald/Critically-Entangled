#
#    This file is part of Critically Entangled.
#    Copyright (c) 2018 Lukas Sägesser, ScyDev GmbH, Switzerland.
#
#    Critically Entangled is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Critically Entangled is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Critically Entangled.  If not, see <http://www.gnu.org/licenses/>.
#

extends Spatial

onready var MAIN = get_node("/root/Main")
onready var CAMERAMAN = get_node("/root/Main/CameraMan")

var minCompassDamageDistance = 40
var cameraAngleCorrection = 25

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

func _on_FlashTimer_timeout():
	self.hide()

func flashDamageCompass(unit):
	if (unit != null):
		var cameraTargetPoint = MAIN.getCameraTargetPoint()
		var cameraDirection = cameraTargetPoint - self.get_translation()
		var cameraAngleCorrectionOffset = cameraDirection.normalized() * self.cameraAngleCorrection
		var directionToUnit = unit.get_translation() - (cameraTargetPoint - cameraAngleCorrectionOffset)
		var distanceFromCameraTarget = directionToUnit.length()
		# flash the compass only if it's a unit around the viewport border
		if (distanceFromCameraTarget > self.minCompassDamageDistance):
			self.look_at(unit.get_translation() - CAMERAMAN.getCamOffset(), Vector3(0, 1, 0))
			
			# keep only rotation of Y axis, because look_at() changes all 3 axis
			var sprite = self.get_node("Sprite3D")
			var rot = sprite.get_rotation()
			rot.x = 0
			rot.z = 0
			sprite.set_rotation(rot)
			
			self.show()
			self.get_node("FlashTimer").start()