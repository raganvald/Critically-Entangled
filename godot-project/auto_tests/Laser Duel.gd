extends Spatial

onready var MAIN = get_node("/root/Main")
onready var HUD = get_node("/root/Main/CameraMan/Camera/HUD")

var runTest = false
var testInitialized = false

var unitOffset = Vector3(0, 0, 0) #Vector3(-50, 0, -50)
var unitDistance = 20

#onready var leftSideUnitType = MAIN.unitPlasmaScene
#onready var leftSideUnitType = MAIN.unitRailScene
#onready var leftSideUnitType = MAIN.unitSlingerScene
onready var leftSideUnitType = MAIN.unitLaserScene

var leftSide = []
var leftSideTier = 1 
var rightSide = []
var rightSideTier = 1

var testTickTimer = null

func _ready():
	if (MAIN.testingMode == true && self.runTest):
		self.testTickTimer = Timer.new()
		self.testTickTimer.connect("timeout", self, "_on_testTickTimer_timeout")
		self.testTickTimer.wait_time = 1.0
		self.add_child(self.testTickTimer)
		self.testTickTimer.start()

func _process(delta):
	# can't run it at ready because world isn't ready then
	if (self.runTest && MAIN.started == true && self.testInitialized == false):
		self.testInitialized = true
		self.initializeTest()

func initializeTest():
		MAIN.playersMainUnit.queue_free()
		for rock in MAIN.get_node("Ground/Obstacles").get_children():
			rock.queue_free()

		###############################################################
		# left side
		var laserT1_L1 = self.leftSideUnitType.instance()
		laserT1_L1.set_translation(self.unitOffset + Vector3(0, 0, 0 * self.unitDistance))
		MAIN.get_node("Units").add_child(laserT1_L1)
		laserT1_L1.adjustToTier(self.leftSideTier)
		laserT1_L1.health = 10
		leftSide.append(laserT1_L1)

		###############################################################
		# right side
		var laserT1_R1 = MAIN.unitLaserScene.instance()
		laserT1_R1.set_translation(self.unitOffset + Vector3(3 * self.unitDistance, 0, 0 * self.unitDistance))
		MAIN.get_node("Units").add_child(laserT1_R1)
		laserT1_R1.adjustToTier(self.rightSideTier)
		laserT1_R1.health = 14
		rightSide.append(laserT1_R1)


func _on_testTickTimer_timeout():
	if (MAIN.started == true):
		for unit in self.leftSide:
			if (is_instance_valid(unit)):
				var targetPos = unit.get_translation() + Vector3(100, 0, 0)
				unit.prepareWeapon(unit.get_network_master(), targetPos)

		for unit in self.rightSide:
			if (is_instance_valid(unit)):
				var targetPos = unit.get_translation() + Vector3(-100, 0, 0)
				unit.prepareWeapon(unit.get_network_master(), targetPos)

