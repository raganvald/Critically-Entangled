#
#    This file is part of Critically Entangled.
#    Copyright (c) 2018 Lukas Sägesser, ScyDev GmbH, Switzerland.
#
#    Critically Entangled is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Critically Entangled is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Critically Entangled.  If not, see <http://www.gnu.org/licenses/>.
#

extends "res://Unit.gd"

var unitType = "LASER"

var hitscanDamage = 1.0 # per frame

var lastKnownColliderName = null

func _ready():
	self.weaponType = WEAPON_TYPE_HITSCAN
	self.projectileScene = preload("res://Projectile_Slinger.tscn")
	self.weaponName = "laser"
	
	self.maxHealth = 100
	self.health = self.maxHealth
	
	# custom physics overrides for each specific unit
	self.unitMovementSpeed = 60
	self.set_angular_damp(0.999)
	self.set_linear_damp(0.99)
	
	self.projectileDirectionSpawnOffset = 0
	self.projectileSpawnOffset = Vector3(0, 0, 0)
	
	self.weaponCooldown = 0.01
	self.weaponMinRange = 0
	self.weaponMaxRange = 40
	
	self.get_node("ScaleWrapper/Effect_Laser_Beam").hide()
	self.get_node("ScaleWrapper/Effect_Laser_Impact/Particles").emitting = false
	
	self.adjustToTier(self.tier)


func _physics_process(delta):
	pass

func prepareWeapon(player_id, targetPos = null):
	if (self.weaponReady == true):
		self.weaponReady = false
		self.startWeaponCooldownTimer()
		
		# this should be != null only during test
		var mouseTargetPos = targetPos
		if (mouseTargetPos == null):
			mouseTargetPos = MAIN.getMouseTargetPoint()
		
		if (mouseTargetPos != null):
			self.rpc("fireWeapon", player_id, mouseTargetPos)

sync func fireWeapon(player_id, mouseTargetPos):
	print("lasering--------")
	self.weaponFiring = true
	self.get_node("ScaleWrapper/Effect_Laser_Beam").show()
	
	var beamOriginPos = self.get_node("ScaleWrapper").to_global(self.get_node("ScaleWrapper/RayCast").translation) # self.translation + self.get_node("RayCast").translation
	var beamTarget = mouseTargetPos
	beamTarget.y = beamOriginPos.y # laser beam always parallel to the ground. not down or upwards towards camera ray collision point.
	
	# point unit towards target
	self.setDestinationPoint(self.translation)
	self.lookAtTarget(beamTarget) # look at mouse pointer. but the actual ray and beam and hit is calculated from barrel straight ahead.
	
	# calculate the farthest point the laser can reach
	var beamDirection = Vector3(sin(self.rotation.y), 0, cos(self.rotation.y)) * -1 # exact beam direction from RayCast/barrel straight ahead, not angled towards mouse pointer
	var beamFarthestPoint = beamOriginPos + (beamDirection.normalized() * weaponMaxRange)
#	get_node("/root/Main/Utils/TargetMarker").set_translation(beamFarthestPoint)
	
	# find collision of laser beam with an object
	var rayCast = self.get_node("ScaleWrapper/RayCast")
	rayCast.cast_to = rayCast.to_local(beamFarthestPoint) # this target position needs to be relative to the RayCasts own translation. And since the RayCast is a child of the unit, the coordinates need to be local to the unit.
	rayCast.force_raycast_update()
	
	var visualBeamLength = null
	var collisionPoint = null
	
	if (rayCast.is_colliding()):
		var collider = rayCast.get_collider()
		collisionPoint = rayCast.get_collision_point() 
#		get_node("/root/Main/Utils/TargetMarker").set_translation(collisionPoint)
#		print("laser rayCast collider ", collider, " ", collider.name, " at: ",collisionPoint)
		
		var distanceToCollision = (collisionPoint - beamOriginPos).length()
		if (distanceToCollision <= weaponMaxRange):
#			print("beam reaching target")
			
			if (collider.get("gameObjectType") == "UNIT" || collider.get("gameObjectType") == "PROJECTILE"):
				collider.takeDamage(self.hitscanDamage * self.adjustedTier)
			elif (collider.get("gameObjectType") == "ROCK"):
				collider.takeDamage(self.hitscanDamage * self.adjustedTier * MAIN.weaponDamageToRocksMultiplier)
			
			self.get_node("ScaleWrapper/Effect_Laser_Impact").translation = get_node("ScaleWrapper").to_local(collisionPoint)
			self.get_node("ScaleWrapper/Effect_Laser_Impact/Particles").emitting = true
			
			if (collider.has_method("playSound")):
				collider.playSound(self.weaponName+"_impact", true)
		else:
#			print("beam not reaching ...")
			collisionPoint = null # we don't want to use this collisionPoint for anything, cause it's out of range
			self.get_node("ScaleWrapper/Effect_Laser_Impact/Particles").emitting = false
			
			if (collider.has_method("stopSound")):
				collider.stopSound(self.weaponName+"_impact") # stop laser impact sound on target
		
		# stop impact sound on the last target before the current one
		var lastKnownCollider = self.getLastKnownCollider()
		if (lastKnownCollider != null && lastKnownCollider != collider):
			self.stopImpactSoundOnLastKnownCollider()
		
		lastKnownColliderName = collider.get_name()
	else:
#		print("beam not reaching ...")
		self.get_node("ScaleWrapper/Effect_Laser_Impact/Particles").emitting = false
		

		self.stopImpactSoundOnLastKnownCollider()
	
	# calculate beam length to impact (or max reach) inside the scaled ScaleWrapper, so we can use this to scale the sprite. 
	# If we'd use the world space distance inside the ScaleWrapper, it won't match of course.
	if (collisionPoint != null):
		var scaledBeamDistance = (self.get_node("ScaleWrapper").to_local(collisionPoint) - self.get_node("ScaleWrapper").to_local(beamOriginPos)).length()
		visualBeamLength = scaledBeamDistance
	else:
		var scaledBeamDistance = (self.get_node("ScaleWrapper").to_local(beamFarthestPoint) - self.get_node("ScaleWrapper").to_local(beamOriginPos)).length()
		visualBeamLength = scaledBeamDistance
	
	# resize beam visualisation
	var spriteScaleMultiplier = 100.0/256.0 # because the beam sprite is 256px wide. this gives a multiplier that we can use to scale the sprite in 3D space.
	visualBeamLength = visualBeamLength * spriteScaleMultiplier
#	print("visual beam length corrected: ",str(visualBeamLength))
	var laserBeam = self.get_node("ScaleWrapper/Effect_Laser_Beam")
	laserBeam.get_node("Beam").scale.x = visualBeamLength
	
	self.playSound(self.weaponName+"_fire", true)

func getLastKnownCollider():
	if (self.lastKnownColliderName != null):
		var lastKnownCollider = get_node("/root/Main/Units/"+self.lastKnownColliderName)
		if (lastKnownCollider!= null):
			return lastKnownCollider
		else:
			return null
	else:
		return null

func stopImpactSoundOnLastKnownCollider():
	var lastKnownCollider = self.getLastKnownCollider()
	if (lastKnownCollider != null):
		if (lastKnownCollider.has_method("stopSound")):
			lastKnownCollider.stopSound(self.weaponName+"_impact") # stop laser impact sound on old target

sync func stopFiring(player_id):
	if (self.weaponFiring):
		print("stop firing laser")
		self.weaponFiring = false
		self.get_node("ScaleWrapper/Effect_Laser_Beam").hide()
		self.get_node("ScaleWrapper/Effect_Laser_Impact/Particles").emitting = false
		
		self.stopSound(self.weaponName+"_fire")
		self.stopImpactSoundOnLastKnownCollider()

sync func destroyUnit():
	if (!self.destroyed):
		self.stopImpactSoundOnLastKnownCollider()
		
		.destroyUnit()





