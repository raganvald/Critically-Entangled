#
#    This file is part of Critically Entangled.
#    Copyright (c) 2018 Lukas Sägesser, ScyDev GmbH, Switzerland.
#
#    Critically Entangled is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Critically Entangled is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Critically Entangled.  If not, see <http://www.gnu.org/licenses/>.
#

extends Spatial

onready var MAIN = get_node("/root/Main")

var currPlayingStream = null

func _ready():
	for stream in get_children():
		# WARNING! audio files must be set NOT to loop, or finished() signal will never happen!
		stream.connect("finished", self, "_on_song_finished")
	
	if (MAIN.testingMode == false || MAIN.testingModePlayMusic == true):
		self.pickNextSong()
		#playSong()

func pickNextSong():
	if (self.currPlayingStream == null):
		self.currPlayingStream = get_child(0)
	else:
		var currIndex = self.currPlayingStream.get_index()
		self.currPlayingStream = get_child(currIndex+1)
		
		if (self.currPlayingStream == null):
			self.currPlayingStream = get_child(0)
	
	self.currPlayingStream.play()
#	# starting each song a few seconds from the end, to test playlist looping
#	var length = self.currPlayingStream.get_stream().get_length()
#	self.currPlayingStream.play(length-15.0)

func playSong(song):
	self.currPlayingStream = get_node(song)
	get_node(song).play()

func stopSong():
	if (self.currPlayingStream != null):
		self.currPlayingStream.stop()

func _on_song_finished():
	self.pickNextSong()

