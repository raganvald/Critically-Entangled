#
#    This file is part of Critically Entangled.
#    Copyright (c) 2018 Lukas Sägesser, ScyDev GmbH, Switzerland.
#
#    Critically Entangled is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Critically Entangled is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Critically Entangled.  If not, see <http://www.gnu.org/licenses/>.
#

extends Spatial

func _ready():
	pass

func playSound(sampleName):
	var audioStream = load("res://assets/sounds/"+sampleName+".wav")
	
	var audioPlayer = get_node("AudioStreamPlayer3D")
	audioPlayer.set_stream(audioStream) # this restarts playback is player is already playing a sample
	audioPlayer.play()
 
func showParticleEffect(effectName):
	get_node("Particles/"+effectName).show()

func _on_Timer_timeout():
	self.queue_free()
