#
#    This file is part of Critically Entangled.
#    Copyright (c) 2018 Lukas Sägesser, ScyDev GmbH, Switzerland.
#
#    Critically Entangled is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Critically Entangled is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Critically Entangled.  If not, see <http://www.gnu.org/licenses/>.
#

extends "res://Unit.gd"

var unitType = "RAIL"

func _ready():
	self.weaponType = WEAPON_TYPE_PROJECTILE
	self.projectileScene = preload("res://Projectile_Rail.tscn")
	self.weaponName = "rail"
	
	self.maxHealth = 200
	self.health = self.maxHealth
	
	# custom physics overrides for each specific unit
	self.unitMovementSpeed = 20
	self.set_angular_damp(0.999)
	self.set_linear_damp(0.99)
	
	self.weaponCooldown = 5.0
	
	self.adjustToTier(self.tier)

func _physics_process(delta):
	pass
