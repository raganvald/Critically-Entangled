extends Spatial

func _ready():
	doNav()

func  _fixed_process():
	doNav()

func doNav():
	print("run nav test")
	var start = get_node("/root/Spatial/Start").get_translation()
	var destination = get_node("/root/Spatial/Destination").get_translation()
	print("start: ",start," dest: ",destination)
	
	var path = get_node("/root/Spatial/Navigation").get_simple_path(start, destination)
	
	get_node("/root/Spatial/ImmediateGeometry").set_color(Color(1, 0, 0))
	get_node("/root/Spatial/ImmediateGeometry").begin(Mesh.PRIMITIVE_LINES)
	for step in path:
		get_node("/root/Spatial/ImmediateGeometry").add_vertex(step + Vector3(0, 0.3, 0))
	get_node("/root/Spatial/ImmediateGeometry").end()
	
	print("path: ",path);
	